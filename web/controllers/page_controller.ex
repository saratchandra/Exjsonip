defmodule Exjsonip.PageController do
  use Exjsonip.Web, :controller

  plug :get_ip

  def index(conn, _params) do
    render(conn, "home.json", %{ip: conn.assigns[:ip]})
  end

  def about(conn, _params) do
    render(conn, "about.json", %{})
  end

  def user_agent(conn, _params) do
    user_agent = conn
                 |> get_req_header("user-agent")
                 |> Enum.at(0)
    render(conn, "useragent.json", %{ip: conn.assigns[:ip], user_agent: user_agent})
  end

  def headers(conn, _params) do
    headers = conn.req_headers
              |> Enum.into(%{})
    render(conn, "headers.json", %{ip: conn.assigns[:ip], headers: headers})
  end

  # Gets remote ip and assigns to the connection
  defp get_ip(conn, _opts) do
    # A header can be set in config to check for remote ip
    ip_header = Exjsonip.Endpoint.config(:ip_header)
    remote_ip =
      case ip_header do
        false ->
          conn.remote_ip
        _ ->
          # In case a header is set
          header = Exjsonip.Endpoint.config(:ip_header_name) 
                   |> String.lowercase()
          conn 
          |> get_req_header(header)
      end
    conn
    |> assign(:ip, remote_ip)
  end
end