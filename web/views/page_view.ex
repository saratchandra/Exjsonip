defmodule Exjsonip.PageView do
  use Exjsonip.Web, :view

  def render("home.json", %{ip: {a, b, c, d}}) do
  	%{ip: "#{a}.#{b}.#{c}.#{d}", about: "/about"}
  end

  def render("about.json", %{}) do
  	%{about: "This service gives you the IP address, user agent and request header of the request"}
  end

  def render("useragent.json", %{ip: {a, b, c, d}, user_agent: user_agent}) do
  	%{ip: "#{a}.#{b}.#{c}.#{d}", user_agent: user_agent}
  end

  def render("headers.json", %{ip: {a,b,c,d}, headers: headers}) do
    %{ip: "#{a}.#{b}.#{c}.#{d}", headers: headers}
  end
end
