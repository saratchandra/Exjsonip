defmodule Exjsonip.Router do
  use Exjsonip.Web, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", Exjsonip do
    pipe_through :api

    get "/", PageController, :index
    get "/about", PageController, :about
    get "/useragent", PageController, :user_agent
    get "/headers", PageController, :headers
  end
end
