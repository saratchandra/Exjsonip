# Exjsonip

This is an api service to provide IP, Headers and User Agent for the requested user. Inspired by httpbin.org



To start Exjsonip:

  1. Install dependencies with `mix deps.get`
  2. Start Exjsonip with `mix phoenix.server`